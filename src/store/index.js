import { createStore } from 'vuex'

export default createStore({
  state() {
    return {
      mainView: ''
    };
  },
  mutations: {
    changeView(state, view) {
      state.mainView = view;
    }
  },
  actions: {
  },
  modules: {
  }
})
